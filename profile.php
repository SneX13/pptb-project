<?php
require_once 'core/init.php';
$user = new User();

if (!$user->isLoggedIn()) {
  Redirect::to('index');
} else if (!$user->exists()) {
  Redirect::to(404);
} else {
  $data = $user->data();
}
?>
<html>
<?php include 'includes/content/head.php'; ?>
  <body>
    <header>

      <div class="flex-container">
        <p class="flex-item-1">Dobrodošli: <?php echo escape($user->data()->ime); ?></p>
        <h1 class="flex-item-2">Program praćenja tova brojlera</h1>
        <p class="flex-item-3">Farma: <?php echo escape($user->data()->naziv); ?></p>
        <p class="logout"><a href="logout.php">Odjavi se</a></p>
      </div>
    </header>
    <div id="wrapper-record">
      <div>
        <br/>
        <a href="index.php">&larr; Povratak na prethodnu stranu</a>
        <br/><br/>
      </div>
      <div class="menu-main">
<?php
include 'includes/content/menu.php';
echo Navigation::GenerateMenu($menuProfile, $class);
?>
      </div>
      <main>
        <div class="content-main">
<?php

if (Session::exists('profile')) {
  echo '<p>' . Session::flash('profile') . '</p>';
}

if (empty($_GET['p'])) {
  include 'includes/content/default-user.php';
} else {
  switch ($_GET['p']) {
    case 'podaci':
      include 'includes/content/update.php';
      break;
    case 'lozinka':
      include 'includes/content/changepassword.php';
      break;
  }
}
?>
        </div>
      </main>
    </div>

<?php include 'includes/content/footer.php'; ?>

  </body>
</html>