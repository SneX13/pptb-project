<?php
if (Input::exists()) {
  if (Token::check(Input::get('token'))) {
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'ime' => array(
            'required' => true,
            'min' => 2,
            'max' => 30,
            'special' => true
        ),
        'prezime' => array(
            'required' => true,
            'max' => 60,
            'special' => true
        ),
        'farma' => array(
            'required' => true,
            'max' => 50,
            'special' => true
        ),
        'adresa' => array(
            'required' => true,
            'special' => true
        ),
        'bpg' => array(
            'required' => true,
            'number' => true,
            'max' => 12,
            'special' => true
        )
    ));

    if ($validation->passed()) {
      try {
        $user->update(array(
            'ime' => Input::get('ime'),
            'prezime' => Input::get('prezime'),
            'naziv' => Input::get('farma'),
            'adresa' => Input::get('adresa'),
            'BPG' => Input::get('bpg')
        ));
        Session::flash('profile', 'Vaši podaci su promenjeni');
        Redirect::to('profile');
      } catch (Exception $e) {
        die($e->getMessage());
      }
    }
  } else {
    foreach ($validation->errors() as $error) {
      echo $error, '<br/>';
    }
  }
}
?>
<form action="" method="POST" class="form-signin">

  <label for="ime">Ime</label>
  <input type="text" name="ime" id="ime" value="<?php echo escape($user->data()->ime); ?>" class="form-control">

  <label for="prezime">Prezime</label>
  <input type="text" name="prezime" id="prezime" value="<?php echo escape($user->data()->prezime); ?>" class="form-control">

  <label for="farma">Naziv farme</label>
  <input type="text" name="farma" id="farma" value="<?php echo escape($user->data()->naziv); ?>" class="form-control">

  <label for="adresa">Adresa farme:</label>
  <input type="text"  id="adresa" name="adresa" value="<?php echo escape($user->data()->adresa); ?>" class="form-control">

  <label for="bpg">Broj poljoprivrednog gazdinstva:</label>
  <input type="text"  id="bpg" name="bpg" value="<?php echo escape($user->data()->BPG); ?>" class="form-control"><br/>

  <input type="submit" name="izmeni" value="Izmeni" class="btn btn-lg btn-primary btn-block" > 
  <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">

</form>