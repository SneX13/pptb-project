<?php
if (Input::exists()) {
  if (Token::check(Input::get('token'))) {
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'lozinka' => array(
            'required' => true,
            'min' => 8
        ),
        'nova-lozinka' => array(
            'required' => true,
            'min' => 8
        ),
        'nova-lozinka-ponovo' => array(
            'required' => true,
            'min' => 8,
            'matches' => 'nova-lozinka'
        )
    ));
    if($validation->passed()) {
      if(Hash::make(Input::get('lozinka'),$user->data()->salt) !== $user->data()->lozinka) {
        echo 'Vaša trenutna lozinka je pogrešna';
      } else {
        $salt = Hash::salt(32);
        $user->update(array(
            'lozinka' => Hash::make(Input::get('nova-lozinka'), $salt),
            'salt' => $salt
        ));
        Session::flash('profile', 'Vaša lozinka je promenjena');
        Redirect::to('profile');
      }
    } else {
      foreach ($validation->errors() as $error) {
        echo $error, '<br/>';
      }
    }
  }
}
?>

<form action="" method="POST"  class="form-signin">
    <label for="lozinka">Sadašnja lozinka</label>
    <input type="password" name="lozinka" id="lozinka" class="form-control">
  
    <label for="lozinka_nova">Nova lozinka</label>
    <input type="password" name="nova-lozinka" id="lozinka_nova" class="form-control">
  
    <label for="password_new_again">Nova lozinka ponovo</label>
    <input type="password" name="nova-lozinka-ponovo" id="password_new_again" class="form-control">
  
  <input type="submit" name="izmeni" value="Izmeni" class="btn btn-lg btn-primary btn-block" > 
  <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
</form>