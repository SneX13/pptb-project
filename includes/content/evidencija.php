<?php
$db = new DB();
$rez = $db->query("SELECT turnus_id FROM turnusi ORDER BY turnus_id DESC LIMIT 1")->results();

foreach ($rez as $red) {
  $turnus = $red->turnus_id;
  $dan = 0;
}

if (Input::exists()) {
  $validate = new Validate();
  $validation = $validate->check($_POST, array(
      'datum' => array(
          'required' => true
      ),
      'komada' => array(
          'number' => true,
          'required' => true
      ),
      'uginuca' => array(
          'number' => true
      ),
      'skart' => array(
          'number' => true
      ),
      'tezina' => array(
          'number' => true,
          'required' => true
      ),
      'potrosnja_hrane' => array(
          'number' => true,
          'required' => true
      )
  ));
  if ($validation->passed()) {
    $record = new Record();
    try {
      $record->create(array(
          'datum' => Input::get('datum'),
          'uginuca' => Input::get('uginuca'),
          'skart' => Input::get('skart'),
          'komada' => Input::get('komada'),
          'tezina' => Input::get('tezina'),
          'potrosnja_vode' => 0,
          'potrosnja_hrane' => Input::get('potrosnja_hrane'),
          'turnus' => Input::get('turnus'),
          'farma' => Input::get('farma')
              ), 'evidencije', 'evidencija_id');
      Session::flash('home', 'Uspešno ste se uneli podatke');
    } catch (Exception $e) {
      die($e->getMessage());
      Redirect::to('exception');
    }
  } else {
    $errors = $validation->errors();
  }
}
?>

<p>Broj turnusa: <?php 
if(isset($turnus)){
echo $turnus; }
?></p>
<div >
  <?php
  if (Session::exists('home')) {
    echo '<p>' . Session::flash('home') . '</p>';
  }
  ?>
  <form method="POST" class="" action=''>
    <table class="table table-bordered">
      <tr>
        <th>Datum</th>
        <th>Starost</th>
        <th>Temperatura</th>
        <th>Uginuća</th>
        <th>Škart</th>
        <th>Komada</th>
        <th>Težina</th>
        <th>Potrošnja hrane</th>    
        <th class="no-style"></th>
      </tr>
      <?php
      $rez = $db->query("SELECT * FROM evidencije WHERE turnus='{$turnus}' ORDER by datum ASC")->results();
      foreach ($rez as $red) {

        $farma = $red->farma;
        $datum = $red->datum;
        $datum++;
        $komada = $red->komada;

        $rez1 = $db->query("SELECT * FROM dani_temp WHERE dani='{$dan}' LIMIT 1")->results();
        foreach ($rez1 as $red1) {
          $temperatura = $red1->temperatura;

          echo "
			 <tr>
				<td class=''>{$red->datum}</td>
				<td class=''>{$dan} dan</td>
				<td class=''>{$temperatura} &deg;</td>				
				<td class=''>{$red->uginuca}</td>
				<td class=''>{$red->skart}</td>
                <td class=''>{$red->komada}</td>
				<td class=''>{$red->tezina} grama</td>
				<td class=''>{$red->potrosnja_hrane} kg</td>
				
			  </tr>
			";
          ++$dan;
        }
      }
      ?>
      <?php
      if (isset($error)) {
        
      }
      ?>
      <tr>
        <td class=""><input type="" value="<?php echo $datum; ?>" name="datum" autofocus></td>
        <?php echo (!empty($errors['datum'])) ? $errors['datum'] : ''; ?>

        <td id="starost"><?php echo$dan; ?> dan</td>

        <td id="temp"><?php echo $temperatura; ?> &deg;</td>

        <td class=""><input name='uginuca' value="" id="uginuca" oninput="calculateKom()"></td>
        <?php echo (!empty($errors['uginuca'])) ? $errors['uginuca'] : ''; ?>

        <td class=""><input name='skart' value="" id="skart" oninput="calculateKom()"></td>
        <?php echo (!empty($errors['skart'])) ? $errors['skart'] : ''; ?>

        <td class=""><input name='komada' id="komada"></td>
        <?php echo (!empty($errors['komada'])) ? $errors['komada'] : ''; ?> 

        <td class=""><input name='tezina'>grama</td>
        <?php echo (!empty($errors['tezina'])) ? $errors['tezina'] : ''; ?>

        <td class=""><input name='potrosnja_hrane'>kg</td>
        <?php echo (!empty($errors['potrosnja_hrane'])) ? $errors['potrosnja_hrane'] : ''; ?>

      <input type='hidden' name='turnus' value='<?php echo $turnus; ?>'>
      <input type='hidden' name='farma' value='<?php echo $farma; ?>'>
      <td class="no-border"><input type="submit" class="btn btn-primary btn-lg" name="upisi" value="Upiši"></td>
      </tr>
    </table>
  </form>
</div>
<script>
  function calculateKom() {
    var sumKomada = parseInt('<?php echo $komada; ?>');
    var uginuca = document.getElementById('uginuca').value;
    var skart = document.getElementById('skart').value;
    var rez = document.getElementById('komada');
    var komada = (sumKomada) - (Number(uginuca) + Number(skart));
    rez.value = komada;
  }
</script>