<?php
$class = 'navigation'; 
function Navlist($items) {
   $ref = isset($_GET['p']) && isset($items[$_GET['p']]) ? $_GET['p'] : null;
  if($ref) {
    $items[$ref]['class'] .= 'selected'; 
  }
  return $items; 
}

$menu = array (
    'callback' => 'Navlist',
  'items' => array (
  'evidencija' => array('text' => 'Evidencija', 'url' => '?p=evidencija', 'class'=>null),
    'prijem' => array('text' => 'Prijem', 'url' => '?p=prijem','class'=>null),
    'hrana' => array('text' => 'Hrana', 'url' => '?p=hrana', 'class'=>null),
    'zdravstvena' => array('text' => 'Zdravstvena zaštita', 'url' => '?p=zdravstvena', 'class'=>null),
    'isporuka' => array('text' => 'Isporuka', 'url' => '?p=isporuka', 'class'=>null),
    'finansije' => array('text' => 'Finansije', 'url' => '?p=finansije', 'class'=>null),
    'turnusi' => array('text' => 'Turnusi', 'url' => '?p=turnusi', 'class'=>null),
   ),
     );

$menuProfile = array (
    'callback' => 'Navlist',
    'items' => array (
        'profil' => array ('text' => 'Izmenite podatke', 'url' => '?p=podaci', 'class' =>null),
        'lozinka' => array ('text' => 'Izmenite lozinku', 'url' => '?p=lozinka', 'class' =>null),
    ),
);

$menuAdmin =  array (
    'callback' => 'Navlist',
    'items' => array (
        'backup' => array ('text' => 'Backup baze', 'url' => '?p=backup', 'class' =>null),
        'insert' => array ('text' => 'Unesite korisnika', 'url' => '?p=admin-insert', 'class' =>null),  
        'update' => array ('text' => 'Izmenite korisnika', 'url' => '?p=admin-update', 'class' =>null),
        'delete' => array ('text' => 'Obrišite korisnika', 'url' => '?p=admin-delete', 'class' =>null),
    ),
);
        
class Navigation {
  public static function GenerateMenu($menu, $class) {
    if(isset($menu['callback'])) {
      $items = call_user_func($menu['callback'], $menu['items']);
    }
    $html = "<nav class='$class'>\n";
    foreach($items as $item) {
      $html .= "<a href='{$item['url']}' class='{$item['class']}'>{$item['text']}</a>\n";
    }
    $html .= "</nav>\n";
    return $html;
  }
}