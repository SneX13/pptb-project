<?php
if (Input::exists()) {
  $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'datum' => array(     
            'required' => true
        ),
        'komada' => array(
            'required' => true,
            'number' => true,
            'special' => true
        ),
        'provenienca' => array(
            'required' => true,
            'special' => true
        ),
        'tezina' => array(
            'required' => true,
            'number' => true
        )
    ));
    if ($validation->passed()) {
     $record = new Record();
      
      try {
         $result = $record->create(array(
            'datum_useljenja' => Input::get('datum'),
            'useljeno_DSP' => Input::get('komada'),
            'provenienca' => Input::get('provenienca'),
            'ulazna_tezina' => Input::get('tezina'),
            'farma' => Input::get('farma')
        ), 'turnusi', 'turnus_id');
         $result = $result->first();
         
         $evidencija = $record->create(array(
            'datum' => $result->datum_useljenja,
            'komada' => $result->useljeno_DSP,
            'uginuca' => 0,
            'skart' => 0,
            'tezina' => $result->ulazna_tezina,
             'potrosnja_vode' => 0,
            'potrosnja_hrane' => 0,
            'turnus' => $result->turnus_id,
             'farma' => $result->farma     
        ), 'evidencije', 'evidencija_id');
    Session::flash('home', 'Uspešno ste upisali podatke!');
    Redirect::to('home');
      } catch (Exception $e) {
        die($e->getMessage());
      }
    } else {
      $errors = $validation->errors();
    }
}
?>
<p>Popunite sva polja</p>
<form method="POST" class="form-signin">
  
<?php
        if (isset($error)) {
          
        }
        ?>
  <label for="datum">Datum useljenja</label>
  <input type="date" id="datum" name="datum" value="<?php echo escape(Input::get('datum')); ?>" class="form-control">
  <?php echo (!empty($errors['datum'])) ? $errors['datum'] : '';?> <br/>
  <label for="komada">Useljeno komada</label>
  <input type='text' id="komada" name="komada" value="<?php echo escape(Input::get('komada')); ?>" class="form-control">
  <?php echo (!empty($errors['komada'])) ? $errors['komada'] : '';?> <br/>
  <label for="provenienca">Provenienca</label>
  <input type='text' id="provenienca" name="provenienca" value="<?php echo escape(Input::get('provenienca')); ?>" class="form-control">
  <?php echo (!empty($errors['provenienca'])) ? $errors['provenienca'] : '';?> <br/>
  <label for="tezina">Ulazna težina</label>
  <input type='text' id="tezina" name="tezina" value="<?php echo (Input::get('tezina')); ?>" class="form-control">
  <?php echo (!empty($errors['tezina'])) ? $errors['tezina'] : '';?><br/>
  <input type="hidden" name="farma" value="<?php echo $user->data()->farma_id; ?>">
   
  <button type="submit" class="btn btn-lg btn-primary btn-block" name="upisi">Upiši</button>

</form>