<html>
  <?php include 'includes/content/head.php'; ?>

  <body>
    <div id="wrapper-landing">
      <div class="flex-container">
        <h1 class="flex-item-2">Program praćenja tova brojlera</h1>
      </div>
      <div id="landing-page">
        <form class="form-signin"  method="POST">

          <?php
          include_once 'login.php';
          if (Session::exists('home')) {
            echo '<p>' . Session::flash('home') . '</p>';
          }
          ?>

          <h2 class="form-signin-heading">Prijavite se na sistem</h2>

          <label class="sr-only" for="email">Adresa elektronske pošte</label>
          <input type="email" autofocus=""  placeholder="Email adresa" class="form-control" id="email" name="email"><br/>

          <label class="sr-only" for="lozinka">Lozinka</label>
          <input type="password"  placeholder="Lozinka" class="form-control" id="lozinka" name="lozinka">

          <div class="checkbox">
            <label for="remember">
              <input type="checkbox" name="remember" id="remember"> Zapamti me
            </label>
          </div>
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
          <button type="submit" class="btn btn-lg btn-primary btn-block" name="login" value="Log in">Prijavi me</button></form>
        <!--register section -->
        <div class="landing-register"> 
          <p><a href="register.php">Registrujte se</a></p><br/>
        </div> <!-- register end -->
      </div> <!--landing-page end -->
    </div> <!--wrapper-landing-end-->

    <?php include 'includes/content/footer.php'; ?>

  </body>
</html>