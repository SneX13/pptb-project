<?php
include_once 'core/init.php';
if (Session::exists('home')) {
  echo '<p>' . Session::flash('home') . '</p>';
}
$user = new User();
if (!$user->isLoggedIn()) {
  Redirect::to('index');
} else if (!$user->hasPermission('2')) {
Redirect::to('home');
}
        
?>
<html>
  <?php include 'includes/content/head.php'; ?>
  <body>
    <header>
     
      <div class="flex-container">
<p class="flex-item-1">Dobrodošli: <?php echo escape($user->data()->ime); ?></p>
        <h1 class="flex-item-2">Program praćenja tova brojlera</h1>
        <p class="flex-item-3">Farma: <?php echo escape($user->data()->naziv); ?></p>
        <p class="logout"><a href="logout.php">Odjavi se</a></p>
      </div>
    </header>
    <div id="wrapper-record">
      <div class="menu-main">
         <?php
        include 'includes/content/menu.php';
        echo Navigation::GenerateMenu($menuAdmin, $class);
        ?>
      </div>
      <main>
        <div class="content-main">
           <?php if (Session::exists('home')) {
  echo '<p>' . Session::flash('home') . '</p>';
}
          if (empty($_GET['p'])) {
            include 'includes/content/default-admin.php';
          } else {
            switch ($_GET['p']) {
              case 'backup':
                include 'includes/content/admin-backup.php';
                break;
              case 'admin-insert':
                include 'includes/content/admin-insert.php';
                break;
              case 'admin-update':
                include 'includes/content/admin-update.php';
                break;
              case 'admin-delete':
                include 'includes/content/admin-delete.php';
                break;
            }
          }
          
            ?>
        </div>
      </main>
    </div>

    <?php include 'includes/content/footer.php'; ?>

  </body>
</html>