<?php

require_once 'core/init.php';

$user = new User();
if ($user->isLoggedIn() && $user->hasPermission('2')) {
  Redirect::to('admin');
} else if ($user->isLoggedIn()) {
  Redirect::to('home');
} else {
  include 'includes/content/landingform.php';
}