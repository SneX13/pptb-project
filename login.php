<?php
require_once 'core/init.php';

if (Input::exists()) {
  if (Token::check(Input::get('token'))) {

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'email' => array(
            'required' => true,
            'special-email' => true),
        'lozinka' => array(
            'required' => true,
            'min' => 8,
            'special' => true
            )
    ));
    
    if ($validation->passed()) {
      $user = new User();
      $remember = (Input::get('remember') === 'on') ? true : false;
      $login = $user->login(Input::get('email'), Input::get('lozinka'), $remember);
      if($login) {
        Redirect::to('index');
      } else {
        echo 'Došlo je do greške prilikom prijavljivanja. Unesite ispravne podatke';
      }
    } else {
      foreach ($validation->errors() as $error) {
        echo $error, '<br/>';
      }
    }
  }
}