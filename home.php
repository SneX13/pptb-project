<?php
require_once 'core/init.php';

$user = new User();
if (!$user->isLoggedIn()) {
  Redirect::to('index');
} 
?>
<html>
  <?php include 'includes/content/head.php'; ?>
  <body>
    <header>

      <div class="flex-container">
        <p class="flex-item-1">Dobrodošli: <a href="profile.php?user=<?php echo hash('md5',$user->data()->email); ?>"><?php echo escape($user->data()->ime); ?> </a></p>
        <h1 class="flex-item-2">Program praćenja tova brojlera</h1>
        <p class="flex-item-3">Farma: <?php echo escape($user->data()->naziv); ?></p>
        <p class="logout"><a href="logout.php">Odjavi se</a></p>
      </div>
    </header>
    <div id="wrapper-record">
      <div class="menu-main">
        <?php
        include 'includes/content/menu.php';
        echo Navigation::GenerateMenu($menu, $class);
        ?>
      </div>
      <main>
        <div class="content-main">
         <?php if (Session::exists('home')) {
  echo '<p>' . Session::flash('home') . '</p>';
}?>
          <?php
          if (empty($_GET['p'])) {
            include 'includes/content/default.php';
          } else {
            switch ($_GET['p']) {
              case 'evidencija':
                include 'includes/content/evidencija.php';
                break;
              case 'prijem':
                include 'includes/content/prijem.php';
                break;
              case 'hrana':
                include 'includes/content/hrana.php';
                break;
              case 'zdravstvena':
                include 'includes/content/zastita.php';
                break;
              case 'isporuka':
                include 'includes/content/isporuka.php';
                break;
              case 'finansije':
                include 'includes/content/finansije.php';
                break;
              case 'turnusi':
                include 'includes/content/turnusi.php';
                break;
            }
          }
          ?>
        </div>
      </main>
    </div>

    <?php include 'includes/content/footer.php'; ?>

  </body>
</html>