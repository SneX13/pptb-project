<?php

class User {

  private $_db,
          $_data,
          $_sessionName,
          $_cookieName,
          $_isLoggedIn;

  public function __construct($user = null) {
    $this->_db = DB::getInstance();

    $this->_sessionName = Config::get('session/session_name');
    $this->_cookieName = Config::get('remember/cookie_name');
    if (!$user) {
      if (Session::exists($this->_sessionName)) {
        $user = Session::get($this->_sessionName);

        if ($this->find($user)) {
          $this->_isLoggedIn = true;
        } else {
          $this->logout($user);
        }
      }
    } else {
      $this->find($user);
    }
  }

  public function update($fields = array(), $id = null) {
    if (!$id && $this->isLoggedIn()) {
      $id = $this->data()->farma_id;
    }


    if (!$this->_db->update('farme', $id, $fields)) {
      throw new Exception('Problem prilikom izmene podataka');
    }
  }

  public function create($fields = array(),$record = 'farme', $id = 'id') {
    if (!$this->_db->insert($record, $fields, $id)) {
      throw new Exception('problem');
    }
  }

  public function find($user = null) {
    if ($user) {
      $field = (is_numeric($user)) ? 'farma_id' : 'email';
      $data = $this->_db->get('farme', array($field, '=', $user));
      if ($data->count()) {
        $this->_data = $data->first();
        return true;
      }
    }
    return false;
  }

  public function login($email = null, $lozinka = null, $remember = false) {

    if (!$email && !$lozinka && $this->exists()) {
      Session::put($this->_sessionName, $this->data()->farma_id);
    } else {
      $user = $this->find($email);

      if ($user) {
        if ($this->data()->lozinka === Hash::make($lozinka, $this->data()->salt)) {
          Session::put($this->_sessionName, $this->data()->farma_id);

          if ($remember) {
            $hash = Hash::unique();
            $hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->farma_id));

            if (!$hashCheck->count()) {
              $this->_db->insert('users_session', array(
                  'user_id' => $this->data()->farma_id,
                  'hash' => $hash
              ));
            } else {
              $hash = $hashCheck->first()->hash;
            }
            Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
          }
          return true;
        }
      }
    }
    return false;
  }

  public function hasPermission($key) {
    if (!empty($this->data()->status) && $this->data()->status == $key) {
      return true;
    } else {
      return false;
    }
  }

  public function exists() {
    return (!empty($this->_data)) ? true : false;
  }

  public function logout() {
    $this->_db->delete('users_session', array('user_id', '=', $this->data()->farma_id));

    Session::delete($this->_sessionName);
    Cookie::delete($this->_cookieName);
  }

  public function data() {
    return $this->_data;
  }

  public function isLoggedIn() {
    return $this->_isLoggedIn;
  }

}
