<?php

class Record {
  
   private $_db;
   
    public function __construct($record = null) {
    $this->_db = DB::getInstance();
    }
 
  public function create($fields = array(), $record = 'turnusi', $id = 'id') {
    $result = $this->_db->insert($record, $fields, $id);
    if (!$result) {
      throw new Exception('Problem prilikom unosa podataka');
    } else {
        return $result;
    }
  }
  public function showRecord($action, $table, $where = array()) {
    $result = $this->_db->action($action, $table, $where = array());
    if(!$result) {
      throw new Exception('Problem prilikom prikaza podataka');
    } else {
      return $result;
    }
  }
  
  public function createEvidencija($turnus) {
      
  }
}