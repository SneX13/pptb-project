<?php

class Validate {

  private $_passed = false,
          $_errors = array(),
          $_db = null;

  public function __construct() {
    $this->_db = DB::getInstance();
  }

  public function check($source, $items = array()) {
    foreach ($items as $item => $rules) {
      foreach ($rules as $rule => $rule_value) {
        $value = trim($source[$item]);
        $item = escape($item);

        if ($rule === 'required' && empty($value)) {
          $this->addError($item, "Polje {$item} je obavezno! <br/>");
        } else if (!empty($value)) {
          switch ($rule) {
            case 'min':
              if (strlen($value) < $rule_value) {
                $this->addError($item, "{$item} mora da bude namanje {$rule_value} karaktera. <br/>");
              }
              break;
            case 'max':
              if (strlen($value) > $rule_value) {
                $this->addError($item, "{$item} može da bude najviše {$rule_value} karaktera. <br/>");
              }
              break;
            case 'unique':
              $check = $this->_db->get($rule_value, array($item, '=', $value));
              if ($check->count()) {
                $this->addError($item, "{$item} već postoji. <br/>");
              }
              break;
            case'matches':
              if ($value != $source[$rule_value]) {
                $this->addError($item, "{$rule_value} mora da bude isto kao {$item} <br/>");
              }
              break;
            case 'number':
              if (!is_numeric($value)) {
                $this->addError($item, "Polje {$item} može da bude samo broj <br/>");
              }
              break;
            case 'special':
              if (preg_match('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $value)) {
                $this->addError($item, "U polje {$item} možete uneti samo slova i brojeve <br/>");
              }
              break;
            case 'special-email':
              if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->addError($item, "Unesite ispravnu Email adresu u polje {$item} <br/>");
              }
              break;
           
          }
        }
      }
    }
    if (empty($this->_errors)) {
      $this->_passed = true;
    }
    return $this;
  }

  private function addError($item, $error) {
    $this->_errors[$item] = $error;
  }

  public function errors() {
    return $this->_errors;
  }

  public function passed() {
    return $this->_passed;
  }

}
