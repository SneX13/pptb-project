<?php
require_once 'core/init.php';

if (Input::exists()) {
  if (Token::check(Input::get('token'))) {
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'ime' => array(
            'required' => true,
            'min' => 2,
            'max' => 30,
            'special' => true
        ),
        'prezime' => array(
            'required' => true,
            'max' => 60,
            'special' => true
        ),
        'farma' => array(
            'required' => true,
            'max' => 50,
            'special' => true
        ),
        'adresa' => array(
            'required' => true,
            'special' => true
        ),
        'bpg' => array(
            'required' => true,
            'max' => 12,
            'number' => true,
            'special' => true
        ),
        'email' => array(
            'unique' => 'farme',
            'special-email' => true
        ),
        'lozinka' => array(
            'required' => true,
            'min' => 8,
            'special' => true
        )
    ));
    if ($validation->passed()) {
      $user = new User();
      $salt = Hash::salt(32);

      try {
        $user->create(array(
            'ime' => Input::get('ime'),
            'prezime' => Input::get('prezime'),
            'naziv' => Input::get('farma'),
            'adresa' => Input::get('adresa'),
            'BPG' => Input::get('bpg'),
            'email' => Input::get('email'),
            'lozinka' => Hash::make(Input::get('lozinka'), $salt),
            'salt' => $salt,
            'status' => 1
        ), 'farme', 'farma_id');
        Session::flash('home', 'Uspešno ste se registrovali, prijavite se');
        Redirect::to('index');
      } catch (Exception $e) {
        die($e->getMessage());
      }
    } else {
      $errors = $validation->errors();
    }
  }
}
?>
<html>
  <?php include 'includes/content/head.php'; ?>
  <body>
    <div id="wrapper-landing">
      <div class="flex-container">
        <h1 class="flex-item-2">Program praćenja tova brojlera</h1>
      </div>
      <a href="index.php">&larr; Povratak na prethodnu stranu</a>
      <div id="landing-page">
        <?php
        if (isset($error)) {
          
        }
        ?>
        <form class="form-signin"   method="POST">
          <h2 class="form-signin-heading">Registracija</h2>

          <p>Popunite sve podatke da biste se registrovali</p>
          <label for="ime">Ime:</label>
          <input type="text" autofocus="" placeholder="Upišite svoje ime" class="form-control" id="ime" name="ime" value="<?php echo escape(Input::get('ime')); ?>"><br/>
          <?php echo (!empty($errors['ime'])) ? $errors['ime'] : ''; ?> <br/>
          <label for="prezime">Prezime:</label>
          <input type="text"  placeholder="Upišite svoje prezime" class="form-control" id="prezime" name="prezime" value="<?php echo escape(Input::get('prezime')); ?>"><br/>
          <?php echo (!empty($errors['prezime'])) ? $errors['prezime'] : ''; ?> <br/>
          <label for="farma">Naziv farme:</label>
          <input type="text"  placeholder="Upišite naziv farme" class="form-control" id="farma" name="farma" value="<?php echo escape(Input::get('farma')); ?>"><br/>
          <?php echo (!empty($errors['farma'])) ? $errors['farma'] : ''; ?> <br/>
          <label for="adresa">Adresa farme:</label>
          <input type="text"  placeholder="Upišite adresu farme" class="form-control" id="adresa" name="adresa" value="<?php echo escape(Input::get('adresa')); ?>"><br/>
          <?php echo (!empty($errors['adresa'])) ? $errors['adresa'] : ''; ?> <br/>
          <label for="bpg">Broj poljoprivrednog gazdinstva:</label>
          <input type="text"  placeholder="Upišite broj poljoprivrednog gazdinstva" class="form-control" id="bpg" name="bpg" value="<?php echo escape(Input::get('bpg')); ?>"><br/>
          <?php echo (!empty($errors['bpg'])) ? $errors['bpg'] : ''; ?> <br/>
          <label for="email">Adresa elektronske pošte:</label>
          <input type="email" autofocus=""  placeholder="Upišite svoju e-mail adresu" class="form-control" id="inputEmail" name="email" value="<?php echo escape(Input::get('email')); ?>"><br/>
          <?php echo (!empty($errors['email'])) ? $errors['email'] : ''; ?> <br/>
          <label for="inputPassword">Lozinka:</label>
          <input type="password"  placeholder="Upišite lozinku" class="form-control" id="inputPassword" name="lozinka">
          <?php echo (!empty($errors['lozinka'])) ? $errors['lozinka'] : ''; ?> <br/>
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
          <button type="submit" class="btn btn-lg btn-primary btn-block" name="signup">Registruj se</button>
        </form>
      </div>
    </div>
    <?php include 'includes/content/footer.php'; ?>

  </body>
</html>